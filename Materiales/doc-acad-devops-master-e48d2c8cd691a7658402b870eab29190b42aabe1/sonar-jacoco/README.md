# Jacoco
Se debe recomineda utilizar el projecto jenkins_getting_started para mostrar el uso básico de Jacoco

# SonarQube
Herramientas usadas durante este curso:
  - SonarQube Server 7.8
  - Java 1.8 
  - Sonar-scanner 3.3.0.1492 For windows

Para la instalación y configuraciones de Sonarqube se sugiere el seguimiento de la documentación oficial:

https://docs.sonarqube.org/7.8/

Asi como la documentacion oficial para sonar-scanner: 
https://docs.sonarqube.org/7.8/analysis/scan/sonarscanner/

Se recomienda implementar dos practicas, una para Configurar la severidad del quality gate y otra para habilitar y deshablitar reglas de sonarqube

